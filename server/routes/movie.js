const express = require('express')
const db = require('../db')
const utils = require('../utils')

const router = express.Router()

router.get('/:title', (request, response) => {
    const { title } = request.params
    const query = `SELECT * FROM Movie WHERE movie_title = '${title}'`

    db.execute(query, (error, result) => {
        response.send(utils.createResult(error, result))
    })
})

router.post('/', (request, response) => {
    const { title, releaseDate, time, dname } = request.body

    const query = `INSERT INTO Movie(movie_title, movie_release_date, movie_time, director_name) VALUES('${title}', '${releaseDate}', '${time}', '${dname}')`

    db.execute(query, (error, result) => {
        response.send(utils.createResult(error, result))
    })
})

router.put('/:id', (request, response) => {
    const { id } = request.params
    const { date, time } = request.body

    const query = `UPDATE Movie SET movie_release_date = '${date}', movie_time = '${time}' WHERE  movie_id = '${id}'`

    db.execute(query, (error, result) => {
        response.send(utils.createResult(error, result))
    })
})

router.delete('/:id', (request, response) => {
    const { id } = request.params

    const query = `DELETE FROM Movie WHERE movie_id = '${id}'`

    db.execute(query, (error, result) => {
        response.send(utils.createResult(error, result))
    })
})

module.exports = router



